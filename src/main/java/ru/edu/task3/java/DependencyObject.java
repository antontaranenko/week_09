package ru.edu.task3.java;

/**
 * ReadOnly
 */
public interface DependencyObject {
    String getValue();
}
