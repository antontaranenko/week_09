package ru.edu.task1.java;

/**
 * ReadOnly
 */
public class ComponentA {

    private ComponentC componentC;

    public ComponentA(ComponentC c) {
        componentC = c;
    }

    public boolean isValid(){
        return componentC != null && componentC.isValid();
    }
}
